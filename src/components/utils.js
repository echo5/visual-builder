const shortid = require('shortid')
const Vue = require('Vue')

/**
 * Get grid sizes in array
 */
exports.getGridPercentages = function (columns) {
   var sizes = [];
   for (var i = 0; i < columns + 1; i++) { 
     sizes[i] = {
     		value: i,
     		percentage: i / columns * 100
     }
   }
   return sizes;
}

/**
 * Find closest value and index in array
 */
exports.getClosestNumber = function(array, targetValue) {
	  var closestNumber = null,
	    closestIndex = null;
	  array.forEach(function(element, index) {
	  	if (closestIndex == null || Math.abs(element.percentage - targetValue) < Math.abs(closestNumber - targetValue)) {
	  	  closestNumber = element.percentage;
	  	  closestIndex = index;
	  	}
	  });
	  return array[closestIndex].value;
}

exports.getAllFieldsFromGroups = function(groups) {
	var schema = {};
	Object.keys(groups).forEach(function(group) {
	  schema = Object.assign({}, groups[group]);
	});
	return schema['fields'];
}

exports.setPosition = function(element, top, left) {
  largestHeight = window.innerHeight - element.offsetHeight - 25;
  largestWidth = window.innerWidth - element.offsetWidth - 25;

  if (top > largestHeight) top = largestHeight;

  if (left > largestWidth) left = largestWidth;

  element.style.top = top + 'px';
  element.style.left = left + 'px';
}

exports.dragStart = function (event) {
	return;
	event.preventDefault();
	return false;
	var eventObj, tagName;
	if (!hasTouch && (event.button === 2 || event.which === 3)) {
		// disable right click
		return;
	}
}

exports.hasClass = function(element, className) {
	return element.className && new RegExp("(^|\\s)" + className + "(\\s|$)").test(element.className);
}

exports.isObject = function(item) {
  return (item && typeof item === 'object' && !Array.isArray(item));
}

exports.isValidSelector = function(selector) {
  var dummy = document.createElement('br');
	try {
		dummy.querySelector(selector);
	} catch (e) {
		return false;
	}
	return true;
};

exports.mergeDeep = function(target, source) {
  let output = Object.assign({}, target);
  if (exports.isObject(target) && exports.isObject(source)) {
    Object.keys(source).forEach(key => {
      if (exports.isObject(source[key])) {
        if (!(key in target))
          Object.assign(output, { [key]: source[key] });
        else
          output[key] = exports.mergeDeep(target[key], source[key]);
      } else {
        Object.assign(output, { [key]: target[key] });
      }
    });
  }
  return output;
}

exports.getParent = function(object, key, children)  {
	var parent = null
	Object.keys(object).forEach(function(item) {
		if (object[item][children] && object[item][children].includes(key)) {
			parent = item;
		}
	})
	return parent
}

exports.arrayToObject = (array, keyField) =>
	array.reduce((obj, item) => {
		obj[item[keyField]] = item
		return obj
	}, {})

exports.ruleSetsArrayToObject = function(items) {

	var objects = {}
	for (var i = 0, length = items.length; i < length; i++) {
		// Rules
		if (items[i]['type'] == 'rule' && items[i]['selectors']) {
			// Split selectors if more than one			
			if (items[i]['selectors'].length > 1) {
				var itemSelectors = items[i]['selectors']
				itemSelectors.forEach(function(itemSelector) {
					var newItem = JSON.parse(JSON.stringify(items[i]))
					objects[itemSelector] = newItem
					objects[itemSelector]['selectors'] = [itemSelector]
				})
			} else {
				objects[items[i]['selectors'][0]] = items[i]
			}
		} 
		// Media queries
		else if (items[i]['type'] == '@media') {
			objects[items[i]['value']] = items[i]
			if (items[i]['nestedRules']) {
				objects[items[i]['value']]['nestedRules'] = this.ruleSetsArrayToObject(items[i]['nestedRules'])
			}
		}
	}

	return objects

	// array.reduce((obj, item) => {
	// 	if (item['selectors']) {
	// 		// Split selectors if more than one			
	// 		if (item['selectors'].length > 1) {
	// 			var itemSelectors = item['selectors']
	// 			for (var selector in item['selectors']) {
	// 				var newItem = item
	// 				console.log(itemSelectors[selector])
	// 				newItem.selectors = [itemSelectors[selector]]
	// 				console.log(newItem)
	// 				obj[itemSelectors[selector]] = newItem
	// 			}
	// 		} else {
	// 			obj[item['selectors']] = 	item
	// 		}
	// 	} else {
	// 		obj[item['value']] = item			
	// 	}
	// 	return obj
	// }, {})

}

exports.nodesToJson = function(htmlString) {

	var template = document.createElement('div');
	var html = htmlString.trim();
	template.innerHTML = html;

	var elements = {
		root: {
			children: []
		}
	}
	if(template.childNodes)
	{
		elements = this.parseChildNodes(elements, template.childNodes, 'root')
	}
	return elements
}

exports.parseChildNodes = function(elements, nodes, parentKey) {

	for(var i=0; i<nodes.length; i++) {
		node = nodes[i];
		var elementKey = shortid.generate()
		console.log(this.$config)
		elements[elementKey] = {
			classes: node.className ? node.className.split(' ') : [],
			type: 'div-element',
			children: [],
			text: node.innerHTML
		}
		elements[parentKey].children.push(elementKey)
		elements = this.parseChildNodes(elements, node.childNodes, elementKey);
	}

	return elements
}

exports.getNodeElementType = function(node) {
	Object.keys(Vue.prototype.$config.elementTypes).forEach(function(elementType){
		if (Vue.prototype.$config.elementTypes[elementType].isType && Vue.prototype.$config.elementTypes[elementType].isType(node))
			return Vue.prototype.$config.elementTypes[elementType].name
	})
}

exports.parseCssSelectorString = function(selector) {
	console.log(selector)
	var obj = {tags:[], classes:[], ids:[], attrs:[], pseudoClasses:[]};
	selector.split(/(?=\.)|(?=#)|(?=\[)|(?=:)/).forEach(function(token){
		switch (token[0]) {
			case '#':
				obj.ids.push(token.slice(1));
				break;
			case '.':
				obj.classes.push(token.slice(1));
				break;
			case '[':
				obj.attrs.push(token.slice(1,-1).split('='));
				break;
			case ':':
				obj.pseudoClasses.push(token.slice(1));
				break;
			default :
				obj.tags.push(token);
				break;
		}
	});
	return obj;
}

exports.permute = function(permutation) {
  var length = permutation.length,
      result = [permutation.slice()],
      c = new Array(length).fill(0),
      i = 1, k, p;

  while (i < length) {
    if (c[i] < i) {
      k = i % 2 && c[i];
      p = permutation[i];
      permutation[i] = permutation[k];
      permutation[k] = p;
      ++c[i];
      i = 1;
      result.push(permutation.slice());
    } else {
      c[i] = 0;
      ++i;
    }
  }
  return result;
}

exports.getSelectorCombinations = function(selector) {
	var combinations = [];
	var prefix = selector.tags.length ? selector.tags[0] : ''
	var suffix = selector.pseudoClasses.length ? ':' + selector.pseudoClasses.join(':') : ''
	var middle = ''
	var middleOptions = []
	var middleOptions = middleOptions.concat(selector.classes.map((value) => '.' + value))
	var middleOptions = middleOptions.concat(selector.ids.map((value) => '#' + value))
	var middleOptions = middleOptions.concat(selector.attrs.map(([key, value]) => '[' + key + '=' + value + ']'))
	var permutations = this.permute(middleOptions)
	permutations.forEach(function(permutation){
		combinations.push(prefix + permutation.join('') + suffix)
	})
	return combinations;
}

exports.getSimilarSelector = function(selector, ruleSets) {
	var parsedSelector = this.parseCssSelectorString(selector)
	var combinations = this.getSelectorCombinations(parsedSelector)
	for (var i=0; i < combinations.length; i++) {
		if (ruleSets[combinations[i]]) {
			selector = combinations[i]
			break
		}
	}
	return selector
}
