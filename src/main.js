import './assets/scss/main.scss';

import Vue from 'vue'
import App from './App.vue'
import store from './store'
import utils from './components/utils.js'

// Merging mixins
import MergeStrategy from './mixins/mergeStrategy'

// Third party
import Draggable from 'vuedraggable'
import VueFormGenerator from 'vue-form-generator'
import Multiselect from 'vue-multiselect'
import Resizer from 'vue-resize-handle/bidirectional'
import contextMenu from 'vue-context-menu'
import { Sketch } from 'vue-color'
import Feather from "feather-icons"
import VueCodemirror, { CodeMirror } from 'vue-codemirror'



// Components
import Iframe from './components/Iframe.vue'
import VueFrame from 'vue-frame'
import Element from './components/Element.vue'
import Editable from './components/Editable.vue'
import RightSidebar from './components/RightSidebar.vue'
import StylesForm from './components/StylesForm.vue'
import StylesGenerator from './components/StylesGenerator.vue'
import ElementForm from './components/ElementForm.vue'
import ElementMenu from './components/ElementMenu.vue'
import ElementHighlighter from './components/ElementHighlighter.vue'
import ElementTree from './components/ElementTree.vue'
import ElementTreeItem from './components/ElementTreeItem.vue'
import RuleSetTree from './components/RuleSetTree.vue'
import RuleSetTreeItem from './components/RuleSetTreeItem.vue'
import ContextMenu from './components/ContextMenu.vue'
import Toolbar from './components/Toolbar.vue'
import Importer from './components/Importer.vue'

// Elements
import Section from './elements/Section.vue'
import Columns from './elements/Columns.vue'
import Column from './elements/Column.vue'
import Text from './elements/Text.vue'
import LinkElement from './elements/LinkElement.vue'
import Heading from './elements/Heading.vue'
import ImageElement from './elements/ImageElement.vue'
import DivElement from './elements/DivElement.vue'
import Raw from './elements/Raw.vue'

// Fields
import fieldDimension from "./fields/fieldDimension.vue";
import fieldRadioIcons from "./fields/fieldRadioIcons.vue";
import fieldColorPicker from "./fields/fieldColorPicker.vue";
Vue.component("fieldDimension", fieldDimension);
Vue.component("fieldRadioIcons", fieldRadioIcons);
Vue.component("fieldColorPicker", fieldColorPicker);
Vue.use(VueFormGenerator);

// Builder
Vue.component('draggable', Draggable);
Vue.component('vue-form-generator', VueFormGenerator.component);
Vue.component('multiselect', Multiselect)
Vue.component('color-picker', Sketch)
Vue.use(VueCodemirror)

Vue.component('el', Element);
Vue.component('i-frame', Iframe);
Vue.component('editable', Editable);
Vue.component('resizer', Resizer);
Vue.component('element-form', ElementForm);
Vue.component('right-sidebar', RightSidebar);
Vue.component('styles-form', StylesForm);
Vue.component('styles-generator', StylesGenerator);
Vue.component('element-menu', ElementMenu);
Vue.component('element-highlighter', ElementHighlighter);
Vue.component('element-tree', ElementTree);
Vue.component('element-tree-item', ElementTreeItem);
Vue.component('rule-set-tree', RuleSetTree);
Vue.component('rule-set-tree-item', RuleSetTreeItem);
Vue.component('context-menu', ContextMenu);
Vue.component('toolbar', Toolbar);
Vue.component('importer', Importer);

// Icons
Vue.prototype.$constants = {}
Vue.prototype.$constants.feather = require('feather-icons').icons

Vue.prototype.$utils = utils;
Vue.prototype.$VueFormGenerator = VueFormGenerator;

// Config
Vue.prototype.$config = {
  previewTarget: '#content',
  elementTypes: {
    'section-element': Section,
    'columns': Columns,
    'column': Column,
    'link-element': LinkElement,
    'heading': Heading,
    'image-element': ImageElement,
    'div-element': DivElement,
    'raw': Raw,
  },  
  saveUrl: 'https://localhost:8080/save',
  gridColumns: 12,
  gridColumnName: 'col-md-',
  gridSizes: utils.getGridPercentages(12),
  formOptions: {
    validateAfterLoad: true,
    validateAfterChanged: true
  },
  mediaQueries: [
    {
      width: '468px',
      query: '(max-width: 468px)',      
      name: 'Mobile',
      icon: Vue.prototype.$constants.feather.smartphone.toSvg(),
      order: 2      
    },
    {
      width: '768px',
      query: '(max-width: 768px)',
      name: 'Tablet',
      icon: Vue.prototype.$constants.feather.tablet.toSvg(),
      order: 3
    },
    {
      width: '100%',
      name: 'Desktop',
      icon: Vue.prototype.$constants.feather.monitor.toSvg(),
      default: true
    }
  ]
};

Object.keys(Vue.prototype.$config.elementTypes).forEach( function(value, key){
  Vue.component(value, Vue.prototype.$config.elementTypes[value]);
});


Vue.prototype.$eventHub = new Vue();

export { app, store }
export default {
  init(args) {
    var app = new Vue({
      el: '#app',
      render: h => h(App),
      store,
      data: window.data,
    })
  }
}