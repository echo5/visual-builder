import Vue from 'vue'
import utils from '../components/utils.js'

// Merging mixins
Vue.config.optionMergeStrategies.groups = function (toVal, fromVal) {
  var groups = utils.mergeDeep(fromVal, toVal);
  return groups
}