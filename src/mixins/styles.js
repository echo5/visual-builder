import store from '../store'

export default {
    data() {
        return { 
            tabs: {
                'General': {
                    fields: {
                        'selectors': {
                        type: "vueMultiSelect",    
                        model: "selectors",
                        label: "Selectors",
                        placeholder: "Select selector classes",
                        tagPlaceholder: "Add new selector",
                        required: true, 
                        selectOptions: {
                            hideSelected: true,
                            multiple: true,
                            // trackBy: "name",
                            // label: "name",
                            searchable: true,
                            taggable: true,
                            closeOnSelect: true,
                            allowEmpty: false,
                            internalSearch: true,
                            onNewTag: function(newTag, id, options, value){
                            if (!value){
                                value = []
                            }
                            store.commit('addSelector', newTag);
                            value.push(newTag);
                            }
                        },
                        values: function() {
                            return Object.keys(store.state.selectors)
                        }
                        // values: Object.keys(store.state.selectors)
                        //   values: [
                        //     "column",
                        //     "column-12",
                        //     "red",
                        //   ]
                        },
                    }
                }
            }
        }
    }
}