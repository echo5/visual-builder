import Vue from 'vue'

export default {
    data() {
        return { 
            groups: {
                'Background': {
                    legend: 'Background',
                    open: false,
                    fields: {
                        'background-size':{
                            type: "select",
                            label: "Background Size",
                            model: "background-size",
                            values: function() {
                            return [
                                { id: "auto", name: "Auto" },
                                { id: "cover", name: "Cover" },
                                { id: "contain", name: "Contain" },
                            ]
                            },
                        },
                        'background-attachment':{
                            type: "select",
                            label: "Background Attachment",
                            model: "background-attachment",
                            values: function() {
                            return [
                                { id: "scroll", name: "Scroll" },
                                { id: "fixed", name: "Fixed" },
                                { id: "local", name: "Local" },
                            ]
                            },
                        },
                        'background-image':{
                            type: "image",
                            label: "Background Image",
                            model: "background-image",
                            set: function(model, value) {
                                value = 'url("' + value + '")'
                                console.log(value)
                                Vue.set(model, 'background-image', value)
                            }
                        },
                        'background-color':{
                            type: "colorPicker",
                            label: "Background Color",
                            model: "background-color",
                            default: { hex: '#ffffff' }
                        },
                    }
                }
            }
        }
    }
}