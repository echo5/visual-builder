export default {
    data() {
        return { 
            groups: {
                'Display': {
                    legend: 'Display',
                    open: false,
                    fields: {
                        'display':{
                            type: "select",
                            label: "Display",
                            model: "display",
                            values: function() {
                            return [
                                { id: "block", name: "Block" },
                                { id: "inline", name: "Inline" },
                                { id: "inline-block", name: "Inline Block" },
                                { id: "flex", name: "Flex" },
                            ]
                            },
                            styleClasses: "half",                            
                        },
                        'position':{
                            type: "select",
                            label: "Position",
                            model: "position",
                            values: function() {
                            return [
                                { id: "relative", name: "Relative" },
                                { id: "absolute", name: "Absolute" },
                                { id: "static", name: "Static" },
                                { id: "fixed", name: "Fixed" },
                            ]
                            },
                            styleClasses: "half last",                            
                        },
                        'left':{
                            type: "dimension",
                            defaultUnit: "px",
                            label: "Left",
                            model: "left",
                            styleClasses: "half",
                        },
                        'right':{
                            type: "dimension",
                            defaultUnit: "px",
                            label: "Right",
                            model: "right",
                            styleClasses: "half last",   
                        },
                        'top':{
                            type: "dimension",
                            defaultUnit: "px",
                            label: "Top",
                            model: "top",
                            styleClasses: "half",
                        },
                        'bottom':{
                            type: "dimension",
                            defaultUnit: "px",
                            label: "Bottom",
                            model: "bottom",
                            styleClasses: "half last",
                        },
                    }
                }
            }
        }
    }
}