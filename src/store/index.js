import Vue from 'vue'
import Vuex from 'vuex'
import VueFormGenerator from 'vue-form-generator'
import utils from '../components/utils.js'
import StylesForm from '../components/StylesForm.vue'
import Specificity from 'specificity'
import shortid from 'shortid'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    elements: window.elements, 
    ruleSets: null, 
    showElementModal: false,
    elementFormFixed: false,
    styleSidebarClosed: false,
    elementMenuOpen: false,
    importModalOpen: false,
    currentSelector: null,
    currentPseudoClass: null,
    currentElementKey: null,
    currentElement: null,
    currentElementNode: null,
    openElementTreeItems: {},
    openRuleSetTreeItems: {},
    highlightedElements: [],
    showContextMenu: false,
    currentTab: 'styles',
    contextMenuPosition: {
      top: '0px',
      left: '0px',
    },
    iframePreview: null,
    styleNode: null,
    currentMediaQuery: null,
    isExporting: false,
  },
  getters: {
    ruleSets: state => {
      return state.ruleSets
    },
    currentSelector: state => {
      return state.currentSelector
    },
    currentRuleSet: state => {
      if (state.currentSelector) {
        if (state.currentMediaQuery && state.currentMediaQuery.query) {
          store.commit('addMediaQuery', state.currentMediaQuery.query)
          store.commit('addNestedRule', [state.currentMediaQuery.query, state.currentSelector])
          return state.ruleSets[state.currentMediaQuery.query].nestedRules[state.currentSelector]
        } else {
          return state.ruleSets[state.currentSelector]
        }
      }
    },
    currentRuleSetDeclarations: state => {
      if (store.getters.currentRuleSet) {
        return store.getters.currentRuleSet.declarations
      }
    },
    currentElementKey: state => {
      return state.currentElementKey
    },
    currentElementNode: state => {
      return state.currentElementNode
    },
    currentElementModel: state => {
      return state.elements[state.currentElementKey]
    },
    currentElementComponent: state => {
      return Vue.options.components[state.elements[state.currentElementKey].type].options
    },
    iframePreview: state => {
      return state.iframePreview
    },
    styleNode: state => {
      return state.styleNode
    },
    cssSelectors: state => {
      var options = []
      Object.keys(store.state.ruleSets).forEach(function(selector){
          if (!selector.startsWith('(')) {
            var selectors = selector.split('.')
            selectors.forEach(function(cssClass) {
                // Remove psuedos and IDs
                cssClass = cssClass.split(':')[0]
                cssClass = cssClass.split('#')[0]
                cssClass = cssClass.trim()
                if (!options.includes(cssClass) && cssClass != '') {
                    options.push(cssClass)
                }
            });
          }

      })
      return options
    },
    currentElementSelectorMatches: (state) => {
      var selectors = []
      if (state.currentElementNode) {
        var el = state.currentElementNode
        Object.keys(store.state.ruleSets).forEach(function(selector){
          if (!selector.startsWith('(')) {
            if (el && el.matches(selector)) {
              selectors.push(selector)
            }
          }
        })
      }
      
      return selectors.sort(Specificity.compare);
    }
  },

  // actions: {
  //   // ensure data for rendering given list type
  //   FETCH_LIST_DATA: ({ commit, dispatch, state }, { type }) => {
  //     commit('SET_ACTIVE_TYPE', { type })
  //     return fetchIdsByType(type)
  //       .then(ids => commit('SET_LIST', { type, ids }))
  //       .then(() => dispatch('ENSURE_ACTIVE_ITEMS'))
  //   },

  //   // ensure all active items are fetched
  //   ENSURE_ACTIVE_ITEMS: ({ dispatch, getters }) => {
  //     return dispatch('FETCH_ITEMS', {
  //       ids: getters.activeIds
  //     })
  //   },

  //   FETCH_ITEMS: ({ commit, state }, { ids }) => {
  //     // only fetch items that we don't already have.
  //     ids = ids.filter(id => !state.items[id])
  //     if (ids.length) {
  //       return fetchItems(ids).then(items => commit('SET_ITEMS', { items }))
  //     } else {
  //       return Promise.resolve()
  //     }
  //   },

  //   FETCH_USER: ({ commit, state }, { id }) => {
  //     return state.users[id]
  //       ? Promise.resolve(state.users[id])
  //       : fetchUser(id).then(user => commit('SET_USER', { user }))
  //   }
  // },

  actions: {
    editElement: ({ commit, state }, [ elementKey, classCycleCounter ]) => {
      commit('setCurrentElementKey', elementKey)
      commit('setCurrentElementNode', state.iframePreview.$el.contentWindow.document.querySelector('[data-vb-element-key="'+ elementKey +'"]'))
      commit('closeElementModal')
      commit('openElementTreeItem', elementKey)      
      var selector = '.' + state.elements[elementKey].classes.length > 0 ? '.' + state.elements[elementKey].classes.join('.') : ''
      selector = utils.getSimilarSelector(selector, state.ruleSets)
      commit('addRuleSet', selector)

      Vue.nextTick(function() {
        var selectorMatches = store.getters.currentElementSelectorMatches
        var slice = (classCycleCounter % selectorMatches.length) * -1
        commit('setCurrentSelector', selectorMatches.sort(Specificity.compare).slice(slice)[0]);          
      })
    }

  },

  mutations: {


    setElements: (state, elements) => {
      state.elements = elements
    },

    setCurrentElementKey: (state, elementKey) => {
      state.currentElementKey = elementKey
    },

    setCurrentElementNode: (state, node) => {
      state.currentElementNode = node
    },

    openElementTreeItem: (state, elementKey) => {
      Vue.set(state.openElementTreeItems, elementKey, true)
      var parent = utils.getParent(state.elements, elementKey, 'children')
      while (parent) {
        Vue.set(state.openElementTreeItems, parent, true) 
        parent = utils.getParent(state.elements, parent, 'children')       
      }
    },

    addElement: (state, [key, element]) => {
      Vue.set(state.elements, key, element)
    },

    createElement: (state,[id,type,properties]) => {
      var groups = Vue.options.components[type].options.groups
      var defaultProperties = {
        type: type,
        children: [],
        classes: []
      }
      var mergedProperties = Object.assign(defaultProperties, properties);
      var newElement = VueFormGenerator.schema.createDefaultObject({fields: groups}, mergedProperties)
      store.commit('addElement', [id, newElement])
    },

    deleteElement: (state, id) => {
      if (state.currentElementKey == id) {
        state.currentElementKey = null
        state.currentElement = null
      }
      // Delete children
      if (state.elements[id].children) {
        for (var i = 0; i < state.elements[id].children.length; i++) {
          store.commit('deleteElement', state.elements[id].children[i])
        }
      }
      // Delete from parent
      var parent = utils.getParent(state.elements, id, 'children')
      if (parent) {
        Vue.delete(state.elements[parent]['children'], state.elements[parent]['children'].indexOf(id))
      }
      // Delete element
      Vue.delete(state.elements, id);
    },

    addChildElement: (state, [parentKey, childKey]) => {
      state.elements[parentKey].children.push(childKey)
    },
    
    setCurrentElement: (state, element) => {
      state.currentElement = element
    },

    setCurrentMediaQuery: (state, query) => {
      state.currentMediaQuery = query   
    },

    setCurrentSelector: (state, selector) => {  
      // Force refresh on item
      var element = state.currentElement
      state.currentElement = null
      state.currentElement = element

      // Set temp to retroactively define if none set
      if (!selector || selector == '' || selector.length < 1) {
        selector = '.vb-temp'
        store.commit('addRuleSet', selector);        
      }

      state.currentSelector = selector
    },

    changeSelectorName: (state, [oldSelector, newSelector]) => {
      if (oldSelector !== newSelector) {
          Object.defineProperty(state.ruleSets, newSelector,
          Object.getOwnPropertyDescriptor(state.ruleSets, oldSelector));
          state.ruleSets[newSelector].selectors[0] = newSelector
          Vue.delete(state.ruleSets, oldSelector);
          Vue.nextTick(function(){
            state.currentSelector = newSelector
          })
      }
    },

    closeModal: (state) => {
      state.showElementModal = false;
    },

    openElementMenu: (state) => {
      state.elementMenuOpen = true;
    },

    openElementModal: (state) => {
      state.showElementModal = true;            
    },

    closeElementMenu: (state) => {
      state.elementMenuOpen = false;
    },

    closeElementModal: (state) => {
      state.elementModalOpen = false;
    },

    toggleElementMenu: (state) => {
      if (state.elementMenuOpen) {
        state.elementMenuOpen = false;
      } else {
        state.elementMenuOpen = true;        
      }
    },

    toggleImportModal: (state) => {
      if (state.importModalOpen) {
        state.importModalOpen = false;
      } else {
        state.importModalOpen = true;        
      }
    },

    changeTab: (state, label) => {
      state.currentTab = label;
    },

    addRuleSet: (state, selector) => {
      if (!state.ruleSets[selector] && utils.isValidSelector(selector)) {
        Vue.set(state.ruleSets, selector, {
          type: 'rule',
          declarations: {},
          selectors: [selector]
        });
      }
    },

    addNestedRule: (state, [parent, selector]) => {
      if (!state.ruleSets[parent].nestedRules[selector] && utils.isValidSelector(selector)) {
        Vue.set(state.ruleSets[parent].nestedRules, selector, {
          type: 'rule',
          declarations: {},
          selectors: [selector]
        });
      }
    },

    addMediaQuery: (state, query) => {
      if (!state.ruleSets[query]) {
        Vue.set(state.ruleSets, query, {
          type: '@media',
          value: [query],
          nestedRules: {},
        });
      }
    },

    setRuleSets: (state, ruleSets) => {
        state.ruleSets = ruleSets;
    },

    updateRuleSet: (state, [selector, property, value]) => {
      Vue.set(state.ruleSets[selector].declarations, property, value);
    },

    setCssClassesFromSelectors: (state) => {
      var options = []
      Object.keys(store.state.ruleSets).forEach(function(selectorRule){
          if (selectorRule.startsWith('.')) {
              // Split multiple classes
              var classes = selectorRule.split(' ')
              classes.forEach(function(cssClass) {
                  // Remove psuedo modifier
                  cssClass = cssClass.split(':')[0].substr(1)
                  if (!options.includes(cssClass)) {
                      options.push(cssClass)
                  }
              });

          }
      })
      state.cssClasses = options
    },

    setIframePreview: (state, iframe) => {
        state.iframePreview = iframe;
    },

    setStyleNode: (state, node) => {
        state.styleNode = node;
    },

    openContextMenu: (state, event) => {
      state.contextMenuPosition.top = event.y
      state.contextMenuPosition.left = event.x
      state.showContextMenu = true;            
    },

    closeContextMenu: (state) => {
      state.showContextMenu = false;            
    },

    startExport: (state) => {
      state.isExporting = true;            
    },

    endExport: (state) => {
      state.isExporting = false;            
    },

  },

})

export default store