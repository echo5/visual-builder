module.exports = {
  init() {
    var app = new Vue({
      el: '#app',
      render: h => h(App),
      store,
      data: window.data
    })
  }
}